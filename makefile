FLAGS := -Wall -Wextra -Wpedantic -Werror

all: build/ibv_sniffer
	@echo "all done"

build/ibv_sniffer: src/ibv_sniffer.c
	mkdir -p build/
	gcc src/ibv_sniffer.c -o build/ibv_sniffer $(FLAGS) -libverbs -g
