#include <errno.h>
#include <infiniband/verbs.h>
#include <stdlib.h>
#include <stdio.h>

#define RING_SIZE 8192
#define MAX_PKT_SIZE 10000

struct ib_config_struct {
	struct ibv_pd* pd;
	struct ibv_cq* cq;
	struct ibv_qp* qp;
	struct ibv_mr* mr;
	
	struct ibv_flow* flow;
	

	void* buffers;
	int buffer_count;
};

void get_ib_device_by_name( const char* name, struct ibv_context** ctx );
void print_all_ib_devices();

void create_input_qp( struct ibv_context* ctx, struct ib_config_struct* cfg );
void create_buffers( struct ib_config_struct* cfg, int count );

/*
 * -L                 : List all ib devices
 * -i <device_name>   : input device
 * -o <device_name>   : output device
 */
int main( int argc, char** argv )
{
	struct ibv_context* i_dev_ctx = NULL;
	struct ibv_context* o_dev_ctx = NULL;

	// ---- Args
	for( int arg_id = 1; arg_id < argc; arg_id++ )
	{
		if( strcmp("-L", argv[arg_id]) == 0 )
		{
			print_all_ib_devices();
			return 0;
		}
		else if( strcmp("-i", argv[arg_id]) == 0 )
		{
			if( i_dev_ctx != NULL )
			{
				printf("Can't use two input devices\n");
			}
			arg_id++;
			if( arg_id >= argc )
			{
				printf("Missing argument after -i\n");
			}


			get_ib_device_by_name( argv[arg_id], &i_dev_ctx );	
		}
		else if( strcmp("-o", argv[arg_id]) == 0 )
		{	
			if( o_dev_ctx != NULL )
			{
				printf("Can't use two output devices\n");
			}	
			arg_id++;
			if( arg_id >= argc )
			{
				printf("Missing argument after -o\n");
			}


			get_ib_device_by_name( argv[arg_id], &o_dev_ctx );	
		}
		else
		{
			printf( "Unknown arg: '%s'\n", argv[arg_id] );
		}
	
	}
		
	// ---- Config check
	if( i_dev_ctx == NULL )
	{
		printf("Provide input device with -i <ib_device_name>\n");
		exit(1);
	}

	// ---- Run
	struct ib_config_struct i_cfg;
	
	create_input_qp( i_dev_ctx, &i_cfg );
	create_buffers( &i_cfg, 2 );

	return 0;
}

void print_all_ib_devices( )
{
	struct ibv_device** list;
	list = ibv_get_device_list( NULL );

	if( list == NULL )
	{
		char buf[100];
		strerror_r( errno, buf, 100 );
		printf( "Error: '%s'\n", buf );
		exit(1);
	}

	int id = 0;
	for(; list[id] != NULL; id++)
	{
		struct ibv_device* dev = list[id];
		printf("%s\n", dev->name );
		printf("%s\n", dev->dev_name );
		printf("%s\n", dev->dev_path );
		printf("%s\n", dev->ibdev_path );
	}

	ibv_free_device_list( list );
}

void get_ib_device_by_name( const char* name, struct ibv_context** ctx )
{
	struct ibv_device** list;
	list = ibv_get_device_list( NULL );

	if( list == NULL )
	{
		char buf[100];
		strerror_r( errno, buf, 100 );
		printf( "Error: '%s'\n", buf );
		exit(1);
	}

	int id = 0;
	for(; list[id] != NULL; id++)
	{
		struct ibv_device* dev = list[id];
		if( strcmp( dev->name, name ) == 0 )
		{
			*ctx = ibv_open_device( dev );
			break;	
		}
	}


	ibv_free_device_list( list );
}

void create_input_qp( struct ibv_context* ctx, struct ib_config_struct* cfg )
{
	struct ibv_qp_init_attr i_attr;
	struct ibv_qp_attr attr;
	struct ibv_flow_attr flow_attr;

	// TODO handle device port
	cfg->pd = ibv_alloc_pd( ctx );
	cfg->cq = ibv_create_cq( ctx, RING_SIZE, NULL, NULL, 0 );	
	
	memset( &i_attr, 0, sizeof i_attr );
	i_attr.recv_cq = cfg->cq;
	i_attr.cap.max_recv_wr = RING_SIZE;
	i_attr.cap.max_recv_sge = 1;
	i_attr.qp_type = IBV_QPT_RAW_PACKET;
	cfg->qp = ibv_create_qp( cfg->pd, &i_attr );

	memset( &attr, 0, sizeof attr );
	attr.qp_state = IBV_QPS_INIT;
	attr.port_num = 1; // ...
	ibv_modify_qp( cfg->qp, &attr, IBV_QP_STATE | IBV_QP_PORT );

	memset( &attr, 0, sizeof attr );
	attr.qp_state = IBV_QPS_RTR;
	ibv_modify_qp( cfg->qp, &attr, IBV_QP_STATE );

	memset( &flow_attr, 0, sizeof flow_attr );
	flow_attr.type = IBV_FLOW_ATTR_SNIFFER;
	flow_attr.size = sizeof flow_attr;
	flow_attr.port = 1; // ...
	cfg->flow = ibv_create_flow( cfg->qp, &flow_attr );
}

void create_buffers( struct ib_config_struct* cfg, int count )
{
	size_t size = RING_SIZE * MAX_PKT_SIZE * count; 
	cfg->buffers = malloc( size );
	cfg->buffer_count = count;

	cfg->mr = ibv_reg_mr( cfg->pd, cfg->buffers, size, IBV_ACCESS_LOCAL_WRITE );
	
}
